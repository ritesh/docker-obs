#! /bin/sh
## Quick steps to configure DoD with OSC:

set -e

OBS_FRONTEND_HOST="${1}"

if [ -z "${OBS_FRONTEND_HOST}" ]; then
	echo "Usage: $0 OBS_FRONTEND_HOST"
	exit 1
fi

osc -A https://${OBS_FRONTEND_HOST} ls
osc -A https://${OBS_FRONTEND_HOST}/ meta prj Debian:8 -c -F data1/test_meta_prj_Debian_8
osc -A https://${OBS_FRONTEND_HOST}/ meta prj Debian:9 -c -F data1/test_meta_prj_Debian_9
osc -A https://${OBS_FRONTEND_HOST}/ meta prjconf Debian:8 -c -F data1/test_meta_prjconf_Debian_8
osc -A https://${OBS_FRONTEND_HOST}/ meta prjconf Debian:9 -c -F data1/test_meta_prjconf_Debian_9
osc -A https://${OBS_FRONTEND_HOST}/ meta prj test -c -F data1/test_meta_prj_test

osc -A https://${OBS_FRONTEND_HOST}/ co test ; cd test
mkdir hello ; cd hello ; apt-get source -d hello ; cd - ;
osc add hello
osc ci -m "New import" hello

echo "Now you may see the hello package is building at https://${OBS_FRONTEND_HOST}/project/show/test"
