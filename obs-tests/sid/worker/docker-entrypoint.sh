#!/usr/bin/env sh

if [ -z "$OBS_SRC_SERVER" -o -z "$OBS_REPO_SERVERS" ]; then
    echo >&2 'error: OBS source server or repository server are unavailable'
    echo >&2 'and hostname option is not specified.'
    echo >&2 '  You need to specify OBS_SRC_SERVER and OBS_REPO_SERVERS.'
    exit 1
fi

if [ -z "$OBS_WORKER_INSTANCES" ]; then
    echo >&2 'error: number of build instances option is not specified.'
    echo >&2 '  You need to specify OBS_WORKER_INSTANCES.'
    exit 1
fi

echo "Configure OBS source server: ${OBS_SRC_SERVER}"
sed -i s/"obs:5352"/"${OBS_SRC_SERVER}"/g /etc/default/obsworker
echo "Configure OBS repository server: ${OBS_REPO_SERVERS}"
sed -i s/"obs:5252"/"${OBS_REPO_SERVERS}"/g /etc/default/obsworker
echo "Number of build instances: ${OBS_WORKER_INSTANCES}"
sed -i s/INSTANCES=\"0\"/INSTANCES=\"${OBS_WORKER_INSTANCES}\"/g /etc/default/obsworker
echo "Starting up OBS worker instances."
service obsworker start

/usr/bin/supervisord -n
